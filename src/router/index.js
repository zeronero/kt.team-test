import Vue from 'vue';
import VueRouter from 'vue-router';
import Todo from '../views/Todo.vue';
import Update from '../views/Update.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Todo,
  },
  {
    path: '/page/:page',
    name: 'page',
    component: Todo,
  },
  {
    path: '/update/:id',
    name: 'update',
    component: Update,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
