import Vue from 'vue';
import Vuex from 'vuex';
import { db, serverTimestamp } from '../firebase/index';


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    list: [],
    error: false,
    len: 0,
  },
  getters: {
    last(state) {
      return state.list[state.list.length - 1];
    },
    length(state) {
      return state.list.length;
    },
    len(state) {
      return state.len;
    },
    list: state => page => state.list.slice(page * 10, page * 10 + 10),
    links: state => [...Array(Math.ceil(state.len / 10)).keys()],
    error: state => state.error,
    byid: state => id => state.list.filter(v => v.id === id),
  },
  mutations: {
    init(state, arr) {
      state.list = arr;
    },
    pull(state, arr) {
      state.list = state.list.concat(arr);
    },
    push(state, v) {
      state.list.unshift(v);
      state.len += 1;
    },
    check(state, id) {
      const item = state.list.find(v => v.id === id);
      if (item !== undefined) {
        item.check = true;
      }
    },
    uncheck(state, id) {
      const item = state.list.find(v => v.id === id);
      if (item !== undefined) {
        item.check = false;
      }
    },
    update(state, obj) {
      const item = state.list.find(v => v.id === obj.id);
      if (item !== undefined) {
        item.text = obj.text;
      }
    },
    delete(state, id) {
      const index = state.list.findIndex(v => v.id === id);
      if (index !== -1) {
        state.list.splice(index, 1);
        state.len -= 1;
      }
    },
    setLen(state, len) {
      state.len = len;
    },
  },
  actions: {
    init(context, arr) {
      context.commit('init', arr);
    },
    pull(context, arr) {
      context.commit('pull', arr);
    },
    push(context, val) {
      db.collection('todo').add({
        check: val.check,
        text: val.text,
        timestamp: serverTimestamp(),
      }).then((v) => {
        val.id = v.id; // eslint-disable-line
        context.commit('push', val);
      }, (error) => {
        console.error('Error writing new message to database', error);
      });
    },
    async check(context, id) {
      await db.collection('todo').doc(id).update({
        check: true,
      });
      context.commit('check', id);
    },
    async uncheck(context, id) {
      await db.collection('todo').doc(id).update({
        check: false,
      });
      context.commit('uncheck', id);
    },
    async update(context, obj) {
      await db.collection('todo').doc(obj.id).update({
        text: obj.text,
      });
      context.commit('update', obj);
    },
    async delete(context, id) {
      await db.collection('todo').doc(id).delete();
      context.commit('delete', id);
    },
    setLen(context, len) {
      context.commit('setLen', len);
    },
  },
});
