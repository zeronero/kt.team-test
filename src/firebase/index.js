import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyAtQkFkB9BqoCDVlESZUbN3ND3DupKa0yc',
  authDomain: 'kt-team-test-e8098.firebaseapp.com',
  databaseURL: 'https://kt-team-test-e8098.firebaseio.com',
  projectId: 'kt-team-test-e8098',
  storageBucket: 'kt-team-test-e8098.appspot.com',
  messagingSenderId: '795294743583',
  appId: '1:795294743583:web:42855b67d6cd7ef089692a',
  measurementId: 'G-9HMG4ZYHDN',
});

export const db = firebase.firestore(); // eslint-disable-line

export const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp; // eslint-disable-line
